#!/usr/bin/env python3

import requests
page=requests.get("https://forecast.weather.gov/MapClick.php?lat=37.7772&lon=-122.4168#.W77-R9czbIV")
#print(page)

from bs4 import BeautifulSoup
soup=BeautifulSoup(page.content,'html.parser')
#print(soup)
seven_day=soup.find(id="seven-day-forecast")
#print(seven_day)
forecast_items=seven_day.find_all(class_="tombstone-container")
#print(forecast_items)
tonight=forecast_items[0]
#print(tonight.prettify())
period=tonight.find(class_="period-name").get_text()
print(period)
short_desc=tonight.find(class_="short-desc").get_text()
print(short_desc)
temp=tonight.find(class_="temp").get_text()
print(temp)
img=tonight.find("img")
desc=img['title']
print(desc)
period_tags=seven_day.select(".tombstone-container .period-name")
periods=[pt.get_text() for pt in period_tags]
#print(periods)
for x in periods:
    print(x)
short_desc=[sd.get_text() for sd in seven_day.select(".tombstone-container .short-desc")]
print(short_desc)
temps=[t.get_text() for t in seven_day.select(".tombstone-container .temp")]
descs=[d["title"] for d in seven_day.select(".tombstone-container .img")]
print(temps)
print(descs)
import pandas as pd 
weather =pd.DataFrame({
    "period":periods,
    "short_desc":short_desc,
    "temp":temps,
    "desc":descs
})
print(weather)